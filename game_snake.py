import pygame
import random
import math

pygame.init()

width, height = 500, 500
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Snake Game")

clock = pygame.time.Clock()

white = (255, 255, 255)
black = (0, 0, 0)
x_head = 20
y_head = 21

apple = (23, 32)

speed_speed = 5
x_speed = 1
y_speed = 0

snake = []
snake_len = 0
snake_len_max = 10

running = True
bum = False

def draw_cube(x, y, color):
    pygame.draw.rect(screen, color, pygame.Rect(x * 10 + 1, y * 10 + 1, 8, 8))

def check_bum(x, y, _snake):
    kaboom = False
    if x <= 0 or x >= width / 10 - 1:
        kaboom = True
    if y <= 0 or y >= height / 10 - 1:
        kaboom = True
    if len(snake) > 1:
        if (x, y) in snake[:-1]:
            kaboom = True
            print('kaboom', (x, y), len(snake), snake)
    return kaboom

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    keys = pygame.key.get_pressed()
    if keys[pygame.K_ESCAPE]:
        running = False
    if keys[pygame.K_LEFT] and x_speed <= 0:
        x_speed = -1
        y_speed = 0
    if keys[pygame.K_RIGHT] and x_speed >= 0:
        x_speed = 1
        y_speed = 0
    if keys[pygame.K_UP] and y_speed <= 0:
        x_speed = 0
        y_speed = -1
    if keys[pygame.K_DOWN] and y_speed >= 0:
        x_speed = 0
        y_speed = 1

    if not check_bum(x_head, y_head, snake):
        x_head += x_speed
        y_head += y_speed


        if (x_head, y_head) == apple:
            snake_len_max += 1
            print("mniami", snake_len_max)
            go_random = True
            while go_random:
                apple = (random.randint(1,width / 10 - 2), random.randint(1,height / 10 -2 ))
                if apple not in snake:
                    go_random = False

        draw_cube(apple[0], apple[1], white)

        snake.append((x_head, y_head))
        snake_len += 1
        if int(snake_len) > snake_len_max:
            point_to_remove = snake.pop(0)
            snake_len -= 1
            draw_cube(point_to_remove[0], point_to_remove[1], black)
        for point in snake:
            draw_cube(point[0], point[1], white)

        for x in range(0, width // 10):
            draw_cube(x, 0, white)
            draw_cube(x, width // 10 - 1, white)
            draw_cube(0, x, white)
            draw_cube(width // 10 - 1, x, white)
    else:
        font = pygame.font.SysFont(None, 78)
        img = font.render(f'END {snake_len_max}', True, white)
        screen.blit(img, (width / 10 /2 , width / 10 / 2))

    pygame.display.flip()
    clock.tick(10 + snake_len_max // 10)

pygame.quit()